User Manual - MultiStepTool
====================================
![](images/vecto_multistage_large.png)\
\

23. November 2021 



This User Manual consists of 3 Parts:

- [User Interface Overview](#user-interface-overview):
    : Gives a short overview of the UI.
- [Create a new Job](#create-a-new-job):
    : Describes how to create new job for the
    - General Case
    - Special Case I
    - Special Case II
- [Create a Step Input File](#create-a-step-input-file):
    : Describes how to create a new step input file.
- [Airdrag](#airdrag):
    : Describes how to load an airdrag component.
