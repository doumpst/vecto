﻿

<Serializable()>
Public Class VectoInputs

Implements IVectoInputs

    Public Property Cycle As String Implements IVectoInputs.Cycle

    Public Property VehicleWeightKG As Single Implements IVectoInputs.VehicleWeightKG

    Public Property PowerNetVoltage As Single Implements IVectoInputs.PowerNetVoltage

    Public Property CycleDurationMinutes As Single Implements IVectoInputs.CycleDurationMinutes

    Public Property FuelMap As String Implements IVectoInputs.FuelMap

End Class

