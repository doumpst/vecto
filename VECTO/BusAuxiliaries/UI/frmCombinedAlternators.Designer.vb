﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCombinedAlternators
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
     
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txt2K10Efficiency = New System.Windows.Forms.TextBox()
        Me.txt2KMaxEfficiency = New System.Windows.Forms.TextBox()
        Me.txt2KMax2Efficiency = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPulleyRatio = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.grpTable2000PRM = New System.Windows.Forms.GroupBox()
        Me.grpTable4000PRM = New System.Windows.Forms.GroupBox()
        Me.txt4KMaxEfficiency = New System.Windows.Forms.TextBox()
        Me.txt4K10Efficiency = New System.Windows.Forms.TextBox()
        Me.txt4KMax2Efficiency = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.grpTable6000PRM = New System.Windows.Forms.GroupBox()
        Me.txt6KMaxEfficiency = New System.Windows.Forms.TextBox()
        Me.txt6K10Efficiency = New System.Windows.Forms.TextBox()
        Me.txt6KMax2Efficiency = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabInputs = New System.Windows.Forms.TabPage()
        Me.txtAlternatorName = New System.Windows.Forms.TextBox()
        Me.lblAlternatorName = New System.Windows.Forms.Label()
        Me.btnClearForm = New System.Windows.Forms.Button()
        Me.gvAlternators = New System.Windows.Forms.DataGridView()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtIndex = New System.Windows.Forms.TextBox()
        Me.tabDiagnostics = New System.Windows.Forms.TabPage()
        Me.txtDiagnostics = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txt2K10Amps = New System.Windows.Forms.TextBox()
        Me.txt2KMax2Amps = New System.Windows.Forms.TextBox()
        Me.txt2KMaxAmps = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt4K10Amps = New System.Windows.Forms.TextBox()
        Me.txt6K10Amps = New System.Windows.Forms.TextBox()
        Me.txt4KMax2Amps = New System.Windows.Forms.TextBox()
        Me.txt4KMaxAmps = New System.Windows.Forms.TextBox()
        Me.txt6KMax2Amps = New System.Windows.Forms.TextBox()
        Me.txt6KMaxAmps = New System.Windows.Forms.TextBox()
        Me.grpTable2000PRM.SuspendLayout()
        Me.grpTable4000PRM.SuspendLayout()
        Me.grpTable6000PRM.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tabInputs.SuspendLayout()
        CType(Me.gvAlternators, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDiagnostics.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt2K10Efficiency
        '
        Me.txt2K10Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt2K10Efficiency.Location = New System.Drawing.Point(110, 57)
        Me.txt2K10Efficiency.Name = "txt2K10Efficiency"
        Me.txt2K10Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt2K10Efficiency.TabIndex = 40
        '
        'txt2KMaxEfficiency
        '
        Me.txt2KMaxEfficiency.ForeColor = System.Drawing.Color.Black
        Me.txt2KMaxEfficiency.Location = New System.Drawing.Point(110, 115)
        Me.txt2KMaxEfficiency.Name = "txt2KMaxEfficiency"
        Me.txt2KMaxEfficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt2KMaxEfficiency.TabIndex = 60
        '
        'txt2KMax2Efficiency
        '
        Me.txt2KMax2Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt2KMax2Efficiency.Location = New System.Drawing.Point(110, 85)
        Me.txt2KMax2Efficiency.Name = "txt2KMax2Efficiency"
        Me.txt2KMax2Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt2KMax2Efficiency.TabIndex = 50
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(56, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(33, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Amps"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(107, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Efficiency"
        '
        'txtPulleyRatio
        '
        Me.txtPulleyRatio.Location = New System.Drawing.Point(606, 22)
        Me.txtPulleyRatio.Name = "txtPulleyRatio"
        Me.txtPulleyRatio.Size = New System.Drawing.Size(68, 20)
        Me.txtPulleyRatio.TabIndex = 30
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(527, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 27
        Me.Label7.Tag = ""
        Me.Label7.Text = "Pully Ratio"
        '
        'grpTable2000PRM
        '
        Me.grpTable2000PRM.Controls.Add(Me.Label6)
        Me.grpTable2000PRM.Controls.Add(Me.Label5)
        Me.grpTable2000PRM.Controls.Add(Me.txt2KMaxAmps)
        Me.grpTable2000PRM.Controls.Add(Me.txt2KMax2Amps)
        Me.grpTable2000PRM.Controls.Add(Me.txt2K10Amps)
        Me.grpTable2000PRM.Controls.Add(Me.txt2KMaxEfficiency)
        Me.grpTable2000PRM.Controls.Add(Me.txt2K10Efficiency)
        Me.grpTable2000PRM.Controls.Add(Me.txt2KMax2Efficiency)
        Me.grpTable2000PRM.Controls.Add(Me.Label1)
        Me.grpTable2000PRM.Controls.Add(Me.Label2)
        Me.grpTable2000PRM.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.grpTable2000PRM.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.grpTable2000PRM.Location = New System.Drawing.Point(38, 102)
        Me.grpTable2000PRM.Name = "grpTable2000PRM"
        Me.grpTable2000PRM.Size = New System.Drawing.Size(181, 166)
        Me.grpTable2000PRM.TabIndex = 28
        Me.grpTable2000PRM.TabStop = False
        Me.grpTable2000PRM.Text = " [  2000 RPM  ]"
        '
        'grpTable4000PRM
        '
        Me.grpTable4000PRM.Controls.Add(Me.txt4KMaxAmps)
        Me.grpTable4000PRM.Controls.Add(Me.txt4KMax2Amps)
        Me.grpTable4000PRM.Controls.Add(Me.txt4K10Amps)
        Me.grpTable4000PRM.Controls.Add(Me.Label8)
        Me.grpTable4000PRM.Controls.Add(Me.Label9)
        Me.grpTable4000PRM.Controls.Add(Me.txt4KMaxEfficiency)
        Me.grpTable4000PRM.Controls.Add(Me.txt4K10Efficiency)
        Me.grpTable4000PRM.Controls.Add(Me.txt4KMax2Efficiency)
        Me.grpTable4000PRM.Controls.Add(Me.Label3)
        Me.grpTable4000PRM.Controls.Add(Me.Label4)
        Me.grpTable4000PRM.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.grpTable4000PRM.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.grpTable4000PRM.Location = New System.Drawing.Point(264, 102)
        Me.grpTable4000PRM.Name = "grpTable4000PRM"
        Me.grpTable4000PRM.Size = New System.Drawing.Size(183, 166)
        Me.grpTable4000PRM.TabIndex = 31
        Me.grpTable4000PRM.TabStop = False
        Me.grpTable4000PRM.Text = " [  4000 RPM  ]"
        '
        'txt4KMaxEfficiency
        '
        Me.txt4KMaxEfficiency.ForeColor = System.Drawing.Color.Black
        Me.txt4KMaxEfficiency.Location = New System.Drawing.Point(111, 113)
        Me.txt4KMaxEfficiency.Name = "txt4KMaxEfficiency"
        Me.txt4KMaxEfficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt4KMaxEfficiency.TabIndex = 90
        '
        'txt4K10Efficiency
        '
        Me.txt4K10Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt4K10Efficiency.Location = New System.Drawing.Point(111, 57)
        Me.txt4K10Efficiency.Name = "txt4K10Efficiency"
        Me.txt4K10Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt4K10Efficiency.TabIndex = 70
        '
        'txt4KMax2Efficiency
        '
        Me.txt4KMax2Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt4KMax2Efficiency.Location = New System.Drawing.Point(111, 85)
        Me.txt4KMax2Efficiency.Name = "txt4KMax2Efficiency"
        Me.txt4KMax2Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt4KMax2Efficiency.TabIndex = 80
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(57, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Amps"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(108, 30)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Efficiency"
        '
        'grpTable6000PRM
        '
        Me.grpTable6000PRM.Controls.Add(Me.txt6KMaxAmps)
        Me.grpTable6000PRM.Controls.Add(Me.txt6KMax2Amps)
        Me.grpTable6000PRM.Controls.Add(Me.txt6K10Amps)
        Me.grpTable6000PRM.Controls.Add(Me.Label10)
        Me.grpTable6000PRM.Controls.Add(Me.Label11)
        Me.grpTable6000PRM.Controls.Add(Me.txt6KMaxEfficiency)
        Me.grpTable6000PRM.Controls.Add(Me.txt6K10Efficiency)
        Me.grpTable6000PRM.Controls.Add(Me.txt6KMax2Efficiency)
        Me.grpTable6000PRM.Controls.Add(Me.Label12)
        Me.grpTable6000PRM.Controls.Add(Me.Label13)
        Me.grpTable6000PRM.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.grpTable6000PRM.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.grpTable6000PRM.Location = New System.Drawing.Point(491, 102)
        Me.grpTable6000PRM.Name = "grpTable6000PRM"
        Me.grpTable6000PRM.Size = New System.Drawing.Size(183, 166)
        Me.grpTable6000PRM.TabIndex = 32
        Me.grpTable6000PRM.TabStop = False
        Me.grpTable6000PRM.Text = " [  6000 RPM  ]"
        '
        'txt6KMaxEfficiency
        '
        Me.txt6KMaxEfficiency.ForeColor = System.Drawing.Color.Black
        Me.txt6KMaxEfficiency.Location = New System.Drawing.Point(112, 113)
        Me.txt6KMaxEfficiency.Name = "txt6KMaxEfficiency"
        Me.txt6KMaxEfficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt6KMaxEfficiency.TabIndex = 120
        '
        'txt6K10Efficiency
        '
        Me.txt6K10Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt6K10Efficiency.Location = New System.Drawing.Point(112, 57)
        Me.txt6K10Efficiency.Name = "txt6K10Efficiency"
        Me.txt6K10Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt6K10Efficiency.TabIndex = 100
        '
        'txt6KMax2Efficiency
        '
        Me.txt6KMax2Efficiency.ForeColor = System.Drawing.Color.Black
        Me.txt6KMax2Efficiency.Location = New System.Drawing.Point(112, 85)
        Me.txt6KMax2Efficiency.Name = "txt6KMax2Efficiency"
        Me.txt6KMax2Efficiency.Size = New System.Drawing.Size(50, 20)
        Me.txt6KMax2Efficiency.TabIndex = 110
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(58, 30)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(33, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Amps"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(109, 30)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Efficiency"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tabInputs)
        Me.TabControl1.Controls.Add(Me.tabDiagnostics)
        Me.TabControl1.Location = New System.Drawing.Point(42, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(700, 494)
        Me.TabControl1.TabIndex = 33
        '
        'tabInputs
        '
        Me.tabInputs.CausesValidation = False
        Me.tabInputs.Controls.Add(Me.txtAlternatorName)
        Me.tabInputs.Controls.Add(Me.lblAlternatorName)
        Me.tabInputs.Controls.Add(Me.btnClearForm)
        Me.tabInputs.Controls.Add(Me.gvAlternators)
        Me.tabInputs.Controls.Add(Me.Label18)
        Me.tabInputs.Controls.Add(Me.btnUpdate)
        Me.tabInputs.Controls.Add(Me.Label17)
        Me.tabInputs.Controls.Add(Me.txtIndex)
        Me.tabInputs.Controls.Add(Me.grpTable2000PRM)
        Me.tabInputs.Controls.Add(Me.grpTable6000PRM)
        Me.tabInputs.Controls.Add(Me.txtPulleyRatio)
        Me.tabInputs.Controls.Add(Me.grpTable4000PRM)
        Me.tabInputs.Controls.Add(Me.Label7)
        Me.tabInputs.Location = New System.Drawing.Point(4, 22)
        Me.tabInputs.Name = "tabInputs"
        Me.tabInputs.Padding = New System.Windows.Forms.Padding(3)
        Me.tabInputs.Size = New System.Drawing.Size(692, 468)
        Me.tabInputs.TabIndex = 0
        Me.tabInputs.Text = " Inputs "
        Me.tabInputs.UseVisualStyleBackColor = True
        '
        'txtAlternatorName
        '
        Me.txtAlternatorName.Location = New System.Drawing.Point(290, 22)
        Me.txtAlternatorName.Name = "txtAlternatorName"
        Me.txtAlternatorName.Size = New System.Drawing.Size(160, 20)
        Me.txtAlternatorName.TabIndex = 20
        '
        'lblAlternatorName
        '
        Me.lblAlternatorName.AutoSize = True
        Me.lblAlternatorName.Location = New System.Drawing.Point(204, 25)
        Me.lblAlternatorName.Name = "lblAlternatorName"
        Me.lblAlternatorName.Size = New System.Drawing.Size(80, 13)
        Me.lblAlternatorName.TabIndex = 40
        Me.lblAlternatorName.Text = "AlternatorName"
        '
        'btnClearForm
        '
        Me.btnClearForm.Location = New System.Drawing.Point(599, 61)
        Me.btnClearForm.Name = "btnClearForm"
        Me.btnClearForm.Size = New System.Drawing.Size(75, 23)
        Me.btnClearForm.TabIndex = 39
        Me.btnClearForm.Text = "Clear Form"
        Me.btnClearForm.UseVisualStyleBackColor = True
        '
        'gvAlternators
        '
        Me.gvAlternators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvAlternators.Location = New System.Drawing.Point(38, 306)
        Me.gvAlternators.Name = "gvAlternators"
        Me.gvAlternators.Size = New System.Drawing.Size(419, 140)
        Me.gvAlternators.TabIndex = 38
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label18.Location = New System.Drawing.Point(38, 288)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(57, 13)
        Me.Label18.TabIndex = 37
        Me.Label18.Text = "Alternators"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(509, 61)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 35
        Me.btnUpdate.Text = "Add"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(38, 25)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(33, 13)
        Me.Label17.TabIndex = 34
        Me.Label17.Text = "Index"
        '
        'txtIndex
        '
        Me.txtIndex.Location = New System.Drawing.Point(77, 22)
        Me.txtIndex.Name = "txtIndex"
        Me.txtIndex.ReadOnly = True
        Me.txtIndex.Size = New System.Drawing.Size(47, 20)
        Me.txtIndex.TabIndex = 33
        '
        'tabDiagnostics
        '
        Me.tabDiagnostics.Controls.Add(Me.txtDiagnostics)
        Me.tabDiagnostics.Location = New System.Drawing.Point(4, 22)
        Me.tabDiagnostics.Name = "tabDiagnostics"
        Me.tabDiagnostics.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDiagnostics.Size = New System.Drawing.Size(692, 468)
        Me.tabDiagnostics.TabIndex = 1
        Me.tabDiagnostics.Text = " Diagnostics"
        Me.tabDiagnostics.UseVisualStyleBackColor = True
        '
        'txtDiagnostics
        '
        Me.txtDiagnostics.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiagnostics.Location = New System.Drawing.Point(23, 16)
        Me.txtDiagnostics.Margin = New System.Windows.Forms.Padding(0)
        Me.txtDiagnostics.Multiline = True
        Me.txtDiagnostics.Name = "txtDiagnostics"
        Me.txtDiagnostics.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDiagnostics.Size = New System.Drawing.Size(648, 429)
        Me.txtDiagnostics.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(550, 523)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 34
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(663, 523)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 35
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'txt2K10Amps
        '
        Me.txt2K10Amps.Location = New System.Drawing.Point(54, 57)
        Me.txt2K10Amps.Name = "txt2K10Amps"
        Me.txt2K10Amps.ReadOnly = True
        Me.txt2K10Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt2K10Amps.TabIndex = 61
        Me.txt2K10Amps.Text = "10"
        '
        'txt2KMax2Amps
        '
        Me.txt2KMax2Amps.Location = New System.Drawing.Point(54, 85)
        Me.txt2KMax2Amps.Name = "txt2KMax2Amps"
        Me.txt2KMax2Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt2KMax2Amps.TabIndex = 62
        '
        'txt2KMaxAmps
        '
        Me.txt2KMaxAmps.Location = New System.Drawing.Point(54, 115)
        Me.txt2KMaxAmps.Name = "txt2KMaxAmps"
        Me.txt2KMaxAmps.Size = New System.Drawing.Size(38, 20)
        Me.txt2KMaxAmps.TabIndex = 63
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 64
        Me.Label5.Text = "l_max/2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 115)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 13)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "l_max"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(9, 115)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 92
        Me.Label8.Text = "l_max"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 87)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 91
        Me.Label9.Text = "l_max/2"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 116)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(34, 13)
        Me.Label10.TabIndex = 122
        Me.Label10.Text = "l_max"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 88)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 121
        Me.Label11.Text = "l_max/2"
        '
        'txt4K10Amps
        '
        Me.txt4K10Amps.Location = New System.Drawing.Point(54, 57)
        Me.txt4K10Amps.Name = "txt4K10Amps"
        Me.txt4K10Amps.ReadOnly = True
        Me.txt4K10Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt4K10Amps.TabIndex = 93
        Me.txt4K10Amps.Text = "10"
        '
        'txt6K10Amps
        '
        Me.txt6K10Amps.Location = New System.Drawing.Point(55, 57)
        Me.txt6K10Amps.Name = "txt6K10Amps"
        Me.txt6K10Amps.ReadOnly = True
        Me.txt6K10Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt6K10Amps.TabIndex = 123
        Me.txt6K10Amps.Text = "10"
        '
        'txt4KMax2Amps
        '
        Me.txt4KMax2Amps.Location = New System.Drawing.Point(54, 84)
        Me.txt4KMax2Amps.Name = "txt4KMax2Amps"
        Me.txt4KMax2Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt4KMax2Amps.TabIndex = 94
        '
        'txt4KMaxAmps
        '
        Me.txt4KMaxAmps.Location = New System.Drawing.Point(54, 112)
        Me.txt4KMaxAmps.Name = "txt4KMaxAmps"
        Me.txt4KMaxAmps.Size = New System.Drawing.Size(38, 20)
        Me.txt4KMaxAmps.TabIndex = 95
        '
        'txt6KMax2Amps
        '
        Me.txt6KMax2Amps.Location = New System.Drawing.Point(55, 85)
        Me.txt6KMax2Amps.Name = "txt6KMax2Amps"
        Me.txt6KMax2Amps.Size = New System.Drawing.Size(38, 20)
        Me.txt6KMax2Amps.TabIndex = 124
        '
        'txt6KMaxAmps
        '
        Me.txt6KMaxAmps.Location = New System.Drawing.Point(55, 113)
        Me.txt6KMaxAmps.Name = "txt6KMaxAmps"
        Me.txt6KMaxAmps.Size = New System.Drawing.Size(38, 20)
        Me.txt6KMaxAmps.TabIndex = 125
        '
        'frmCombinedAlternators
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(774, 579)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmCombinedAlternators"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Combined Alternators - ( Excel V03 )"
        Me.grpTable2000PRM.ResumeLayout(False)
        Me.grpTable2000PRM.PerformLayout()
        Me.grpTable4000PRM.ResumeLayout(False)
        Me.grpTable4000PRM.PerformLayout()
        Me.grpTable6000PRM.ResumeLayout(False)
        Me.grpTable6000PRM.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.tabInputs.ResumeLayout(False)
        Me.tabInputs.PerformLayout()
        CType(Me.gvAlternators, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDiagnostics.ResumeLayout(False)
        Me.tabDiagnostics.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt2K10Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt2KMaxEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt2KMax2Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPulleyRatio As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents grpTable2000PRM As System.Windows.Forms.GroupBox
    Friend WithEvents grpTable4000PRM As System.Windows.Forms.GroupBox
    Friend WithEvents txt4KMaxEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt4K10Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt4KMax2Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents grpTable6000PRM As System.Windows.Forms.GroupBox
    Friend WithEvents txt6KMaxEfficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt6K10Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents txt6KMax2Efficiency As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabInputs As System.Windows.Forms.TabPage
    Friend WithEvents gvAlternators As System.Windows.Forms.DataGridView
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtIndex As System.Windows.Forms.TextBox
    Friend WithEvents tabDiagnostics As System.Windows.Forms.TabPage
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnClearForm As System.Windows.Forms.Button
    Friend WithEvents txtAlternatorName As System.Windows.Forms.TextBox
    Friend WithEvents lblAlternatorName As System.Windows.Forms.Label
    Friend WithEvents txtDiagnostics As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt2KMaxAmps As System.Windows.Forms.TextBox
    Friend WithEvents txt2KMax2Amps As System.Windows.Forms.TextBox
    Friend WithEvents txt2K10Amps As System.Windows.Forms.TextBox
    Friend WithEvents txt4KMaxAmps As System.Windows.Forms.TextBox
    Friend WithEvents txt4KMax2Amps As System.Windows.Forms.TextBox
    Friend WithEvents txt4K10Amps As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt6KMaxAmps As System.Windows.Forms.TextBox
    Friend WithEvents txt6KMax2Amps As System.Windows.Forms.TextBox
    Friend WithEvents txt6K10Amps As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
